//Agregar un producto a myproducts eliminando el ingresado de products

const products = [
    {name: 'pizza',price: 12,id: '🍕'},
    {name: 'Burger',price: 23,id: '🍔'},
    {name: 'Hot Dog',price: 34,id: '🌭'},
    {name: 'Hot Cakes',price: 355,id: '🥞'}
]

const myProducts = []
console.log("products", products)
console.log("myProducts", myProducts)
console.log("-".repeat(10))

const productIndex = products.findIndex(item => item.id === '🍔')
productIndex !== -1 ?(
            myProducts.push(products[productIndex]),
            products.splice(productIndex,1)
        ) 
        : (console.log("error"))

console.log("products", products)
console.log("myProducts", myProducts)
console.log("-".repeat(10))


//Modificar un producto de  products2


const products2 = [
    {name: 'pizza',price: 12,id: '🍕'},
    {name: 'Burger',price: 23,id: '🍔'},
    {name: 'Hot Dog',price: 34,id: '🌭'},
    {name: 'Hot Cakes',price: 355,id: '🥞'}
]

const update = {
    id: "🥞",
    changes: {
        price: 200,
        description: 'delicioso'
    }
}

const productIndexV2 = products2.findIndex(item => item.id === update.id)
products2[productIndexV2] = {
    ...products2[productIndexV2],
    ...update.changes,
}

console.log(products2)