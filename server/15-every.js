// Devuelve true si todos los items cumplen una condicion y false si no
const numbers = [1,30,49,29,10,13]

const  rta = numbers.every(item => item <= 40)
console.log('rta', rta)


// reto determinar con every si las personas del array team son mayores a 15
const team = [
    {
        customerName: 'Nicolas',
        age: 20,
    },
    {
        customerName: 'Zulema',
        age: 25,
    },
    {
        customerName: 'Santiago',
        age: 18,
    },
    {
        customerName: 'Valentina',
        age: 60,
    },
]

const edades = team.map(age => age.age)
const rta2 = edades.every(menor => menor > 15)
console.log('rta2', rta2)