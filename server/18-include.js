// include devuelve true  si un hay un elemento dentro de un array o false si no
const pets = ['cat','dog','bat']
const rta = pets.includes('cat')
console.log('rta', rta)

//playground

function solution(word, query) {
    // Tu código aquí 👈
    const wordLower = word.toLowerCase()
    const queryLower = query.toLowerCase()

    return wordLower.includes(queryLower)
}; 

console.log(solution("Hada madrina", "hadda"))