// find me deveulve el primer valor buscado como objeto, filter hace lo mismo pero solo me devuelve un array de todos los elementos conseguidos (asi de 1 solo), find retorna undefined cuando el valor no está

const numbers = [1,30,49,29,10,13]

const rta = numbers.find(item => item === 30)
console.log('find', rta)

const products = [
    {
        name: 'pizza',
        price: 12,
        id: '🍕'
    },
    {
        name: 'Burger',
        price: 23,
        id: '🍔'
    },
    {
        name: 'Hot Dog',
        price: 34,
        id: '🌭'
    },
    {
        name: 'Hot Cakes',
        price: 355,
        id: '🥞'
    }
]

const rta2 = products.find(item => item.id === '🍕')
console.log ('rta2', rta2)

//FindIndex devuelve solo la key de ese objeto dentro del array si no lo encuentra devuelve -1
const rta3 = products.findIndex(item => item.id === '🥞')
console.log ('rta3', rta3)