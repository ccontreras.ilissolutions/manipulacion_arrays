//concat: toma dos array y los unifica en uno nuevo sin alterar los dos anteriores

const elements = [1,1,2,2]
const otherElements = [3,3,4,4]

const rta = elements.concat(otherElements) //creamos un nuevo array usando la referencia de esos 2
console.log('rta', rta)

/*
rta [
  1, 1, 2, 2,
  3, 3, 4, 4
]
 */

const rta2 = elements.push(...otherElements) //modificamos elements
console.log('rta', rta)

/*
rta [
  1, 1, 2, 2,
  3, 3, 4, 4
]
 */