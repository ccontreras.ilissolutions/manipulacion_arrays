//Combina flat y map para separar un atributo  y luego unirlos en un solo array

const users = [
    {user: 1, username: "tom", attributes: ["nice", "cute"]},
    {user: 2, username: "mike", attributes: ["lovely", "graceful"]},
    {user: 3, username: "nico", attributes: ["cute", "sleepy"]},
    {user: 4, username: "maria", attributes: ["bad", "angry"]},
]

const rta = users.flatMap(user => user.attributes)
console.log('rta', rta)

//Salida
/*
rta [
    'nice',   'cute',
    'lovely', 'graceful',
    'cute',   'sleepy',
    'bad',    'angry'
]
*/

//reto
/*

 */

const calendars = {
    primaryCalendar: [
    {
        startDate: new Date(2021, 1, 1, 15),
        endDate: new Date(2021, 1, 1, 15, 30),
        title: "Cita 1",
    },
    {
    startDate: new Date(2021, 1, 1, 17),
        endDate: new Date(2021, 1, 1, 18),
        title: "Cita 2",
    },
    ],
    secondaryCalendar: [
    {
        startDate: new Date(2021, 1, 1, 12),
        endDate: new Date(2021, 1, 1, 12, 30),
        title: "Cita 2",
    },
    {
        startDate: new Date(2021, 1, 1, 9),
        endDate: new Date(2021, 1, 1, 10),
        title: "Cita 4",
    },
    ],
};

const rtaReto = Object.values(calendars).flatMap(item => {
return item.map(date => date.startDate)
})

console.log(rtaReto)
/*
[
    2021-02-01T18:00:00.000Z,
    2021-02-01T20:00:00.000Z,
    2021-02-01T15:00:00.000Z,
    2021-02-01T12:00:00.000Z
]
*/

//Prueba

const array = [
    "Beautiful is better than ugly",
    "Explicit is better than implicit",
    "Simple is better than complex",
    "Complex is better than complicated",
]

function countWords(array) {
    return array.flatMap(line => line.split(' ')).length;
}

console.log(countWords(array))
