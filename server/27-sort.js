// Por Defecto lo ordena por tabla ASCII
months.sort()
console.log(months)
//[ 'Dec', 'Feb', 'Jan', 'March' ]


// Por Defecto lo ordena por tabla ASCII, tendriamos que usar un arrowFunctios para que tome el orden correspondiente
const numbers = [1,30,4,21,100000]
numbers.sort();
console.log(numbers)
//[ 1, 100000, 21, 30, 4 ]
numbers.sort((a,b) => a - b )
console.log(numbers)
//[ 1, 4, 21, 30, 100000 ]
numbers.sort((a,b) => b - a )
console.log(numbers)
//[ 100000, 30, 21, 4, 1 ]



const words = ['réserve', 'premier', 'communiqué', 'café', 'adieu', 'écla']
words.sort();
console.log(words)
//[ 'adieu', 'café', 'communiqué', 'premier', 'réserve', 'écla' ]


//Sort en los objetos se utiliza esa compracion llamando un atributo de los objetos
const orders = [
    {
        customerName: 'Nicolas',
        total: 600,
        delivered: true
    },
    {
        customerName: 'Zulema',
        total: 120,
        delivered: true
    },
    {
        customerName: 'Santiago',
        total: 1840,
        delivered: true
    },
]
orders.sort((a,b) => b.total - a.total) 
console.log(orders)
/*
[
  { customerName: 'Santiago', total: 1840, delivered: true },
  { customerName: 'Nicolas', total: 600, delivered: true },
  { customerName: 'Zulema', total: 120, delivered: true }
]
 */

//Reto Ordernar por fechas con Sort
const orders2 = [
    {
        customerName: 'Nicolas',
        total: 600,
        delivered: true,
        date: new Date(2022-4-10),
    },
    {
        customerName: 'Zulema',
        total: 120,
        delivered: true,
        date: new Date(2021-8-14),

    },
    {
        customerName: 'Santiago',
        total: 1840,
        delivered: true,
        date: new Date(2022-5-10),
    },
]
orders2.sort((a,b) => new Date(b.date) - new Date(a.date)) 
console.log(orders2)