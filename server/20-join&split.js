//Join une los elementos de los array mediante una separacion y retorna un string, si es undefine o null lo convierte en cadena vacioa

const elements = ["fire", "air", "water"]

const rta = elements.join('/')

console.log('rta', rta)

//rta fire/air/water


//Split crear un array y convierte en elemento cada parte del array que lo incluya

const title = 'curso de manipulación de array'
const rta2 = title.split(' ')
console.log ('rta2', rta2)

//rta2 [ 'curso', 'de', 'manipulación', 'de', 'array' ]


const title2 = 'curso/de/manipulación/de/array'
const rta3 = title2.split('/')
console.log ('rta3', rta3)

//rta3 [ 'curso', 'de', 'manipulación', 'de', 'array' ]


//puedo combinar varias propiedades
const finalTitle = title.split(' ').join('-').toLowerCase()
console.log(finalTitle)