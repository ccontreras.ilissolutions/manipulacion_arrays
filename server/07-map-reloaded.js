const orders = [
    {
        customerName: 'Nicolas',
        total: 60,
        delivered: true,
    },
    {
        customerName: 'Zulema',
        total: 120,
        delivered: false,
    },
    {
        customerName: 'Santiago',
        total: 180,
        delivered: true,
    },
    {
        customerName: 'Valentina',
        total: 240,
        delivered: true,
    },
]

console.log('original', orders)
const resultado = orders.map(item => item.total)
console.log('resultado',resultado)

/* original [
    { customerName: 'Nicolas', total: 60, delivered: true },
    { customerName: 'Zulema', total: 120, delivered: false },
    { customerName: 'Santiago', total: 180, delivered: true },
    { customerName: 'Valentina', total: 240, delivered: true }
]
resultado [ 60, 120, 180, 240 ] */

//copio objeto sin alterar el original

const rta2 = orders.map((item) => {
    return {
        ...item,
        tax: .19
    }
})
console.log('rta2',rta2)
console.log('original', orders)

/* rta2 [
    { customerName: 'Nicolas', total: 60, delivered: true, tax: 0.19 },
    { customerName: 'Zulema', total: 120, delivered: false, tax: 0.19 },
    { customerName: 'Santiago', total: 180, delivered: true, tax: 0.19 },
    { customerName: 'Valentina', total: 240, delivered: true, tax: 0.19 }
]
original [
    { customerName: 'Nicolas', total: 60, delivered: true },
    { customerName: 'Zulema', total: 120, delivered: false },
    { customerName: 'Santiago', total: 180, delivered: true },
    { customerName: 'Valentina', total: 240, delivered: true }
] */