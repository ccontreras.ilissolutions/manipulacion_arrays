//Flat convierte todos los arrays del array principal en una sola linea

const matriz = [
    [1,2,3],
    [4,5,6], [1,2, [1,2]],
    [7,8,9] 
]


const rta = matriz.flat(3) //(n) es el numero de niveles
console.log('rta',rta)

/*
rta [
  1, 2, 3, 4, 5, 6,
  1, 2, 1, 2, 7, 8,
  9
]
*/

//Investigar como aplanar un array de array con recursividad
