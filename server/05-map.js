// Map hace una copia de un array y devuelve uno transformado

const letters = ['a','b','c']

const newArray = letters.map(item => item + `++`)
console.log(newArray)

// Resultado: [ 'a++', 'b++', 'c++' ]
