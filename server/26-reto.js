//Agregar un producto a myproducts sin eliminar el ingresado de products


const products = [
    {name: 'pizza',price: 12,id: '🍕'},
    {name: 'Burger',price: 23,id: '🍔'},
    {name: 'Hot Dog',price: 34,id: '🌭'},
    {name: 'Hot Cakes',price: 355,id: '🥞'}
]

const myProducts = []

const product = products.filter( element => element.id === '🍔' )

myProducts.push(product)
console.log("products", products)
console.log("myProducts", myProducts)
console.log("-".repeat(10))

//Modificar un producto de  products2 sin alterar products 2

const products2 = [
    {name: 'pizza',price: 12,id: '🍕'},
    {name: 'Burger',price: 23,id: '🍔'},
    {name: 'Hot Dog',price: 34,id: '🌭'},
    {name: 'Hot Cakes',price: 355,id: '🥞'}
]

const update = {
    id: "🥞",
    changes: {
        price: 200,
        description: 'delicioso'
    }
}

const newArray2 = products2.map(item => {
    if(item.id === update.id){
        return {
            ...item,
            ...update.changes
        }
    }else{
        return {
            ...item
        }
    }
});

console.log(products2)
console.log(newArray2)
