// toma el array e idfentifica cuantas hay segun unidad
const items = [1,3,2,3]

rta = items.reduce((obj, item) => {
    if (!obj[item]){
        obj[item] = 1
    } else {
        obj[item] = obj[item] + 1
    }
    return obj
}, {});

console.log('rta', rta)
// resultado rta { '1': 1, '2': 1, '3': 2 }


const orders = [
    {
        customerName: 'Nicolas',
        nivel: 'Alto',
    },
    {
        customerName: 'Zulema',
        nivel: 'Bajo',
    },
    {
        customerName: 'Santiago',
        nivel: 'Bajo',
    },
    {
        customerName: 'Valentina',
        nivel: 'Expert',
    },
]


const lvl = orders
    .map(item => item.nivel)
    .reduce((obj, item) => {
        if (!obj[item]){
            obj[item] = 1
        } else {
            obj[item] = obj[item] + 1
        }
        return obj
    }, {})
console.log(lvl)

// Arroja: { Alto: 1, Bajo: 2, Expert: 1 }

//Reto:
const arr = [3, 10, 9, 4, 3, 1, 8, 4, 7, 6];
const result = arr.reduce((obj, item) => {
    if (item <= 5) {
        obj['1-5']++
    } else if (item <= 8) {
        obj['6-8']++
    } else {
        obj['9-10']++
    }
    return obj
    }, {
    '1-5': 0,
    '6-8': 0,
    '9-10': 0
    })

console.log(result)

//{ '1-5': 5, '6-8': 3, '9-10': 2 }
