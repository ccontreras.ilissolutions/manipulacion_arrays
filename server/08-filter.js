// filter filtra un resultado de acuerdo a una condición especifica 

const words = ["spray", "limit", "elite", "exuberant"]

const rta = words.filter(item => item.length >=6) // Filter aqui filtra los items que sean > a 6 caracteres

console.log('rta', rta)
console.log('original', words)

/*
Resultado:
rta [ 'exuberant' ]
original [ 'spray', 'limit', 'elite', 'exuberant' ]
*/

const orders = [
    {
        customerName: 'Nicolas',
        total: 60,
        delivered: true,
    },
    {
        customerName: 'Zulema',
        total: 120,
        delivered: false,
    },
    {
        customerName: 'Santiago',
        total: 180,
        delivered: true,
    },
    {
        customerName: 'Valentina',
        total: 240,
        delivered: true,
    },
]

const rta2= orders.filter( entrega => entrega.delivered == true) // aqui filter filtra todos los items que esten con delivered activado
console.log('rta2',rta2)

/* 
Resultado:
rta2 [
    { customerName: 'Nicolas', total: 60, delivered: true },
    { customerName: 'Santiago', total: 180, delivered: true },
    { customerName: 'Valentina', total: 240, delivered: true }
]
*/

const rta3= orders.filter( entrega => entrega.delivered == true && entrega.total >= 100) // items entregados y con un valor total > 100
console.log('rta3',rta3)

/* 
Resultado:
rta3 [
    { customerName: 'Santiago', total: 180, delivered: true },
    { customerName: 'Valentina', total: 240, delivered: true }
]
]
*/


// Aplicativo para buscar en un array en base al nombre del cliente

const search = (query) => {
    return orders.filter(item =>{
        return item.customerName.includes(query)
    })
}

console.log('Busqueda:', search('ti'))

/*
Busqueda: [
    { customerName: 'Santiago', total: 180, delivered: true },
    { customerName: 'Valentina', total: 240, delivered: true }
]
*/
