// devuelve true si la condición es valida o false si no

const numbers = [1,2,3,4]

const rta = numbers.some(item => item % 2 === 0)
console.log('rta', rta)

const orders = [
    {
        customerName: 'Nicolas',
        total: 60,
        delivered: true,
    },
    {
        customerName: 'Zulema',
        total: 120,
        delivered: false,
    },
    {
        customerName: 'Santiago',
        total: 180,
        delivered: true,
    },
    {
        customerName: 'Valentina',
        total: 240,
        delivered: true,
    },
]

const rta2 = orders.some(item => item.delivered)
console.log('rta2', rta2)

const dates = [
    {
        startDate: new Date(2022,1,1,10),
        endDate: new Date(2022,1,1,11),
        title: "Cita de Trabajo",
    },
    {
        startDate: new Date(2022,1,1,15),
        endDate: new Date(2022,1,1,15,30),
        title: "Cita con Jefe",
    },
    {
        startDate: new Date(2022,1,1,20),
        endDate: new Date(2022,1,1,21),
        title: "Cena",
    },
]


// Algoritmo que develve true si una cita cruza con alguna cita anterior y false si esta despejada, (usamos la libreria date-fns usando el modulo areIntervalsOverlapping)
const newAppointment = {
    startDate: new Date(2022,1,1,6),
    endDate: new Date(2022,1,1,7,30),
    title: "Nueva Cita",
}

const areIntervalsOverlapping = require('date-fns/areIntervalsOverlapping')

const isOverlap = (newDate) => {
    return dates.some( date => {
        return areIntervalsOverlapping(
            {start: date.startDate, end: date.endDate},
            {start: newDate.startDate, end: newDate.endDate},
        )
    })
}

console.log('isOverlap?',isOverlap(newAppointment))